# Short guide to Bitbucket page of CSI group #

This document describes how to use [our group page](https://bitbucket.org/csi-dcsc) in Bitbucket (BB). It explains how to log-in, create a new repository in BB, and how to upload the existing code.

		
The document itself is also uploaded to BB and serves as a sand box for learning basic actions on BB.

Please send your feedback to [Oleg](o.a.solviev@tudelft.nl).